'use strict';

const AWS = require('aws-sdk');
const config = AWS.config.loadFromPath('config.json');
const s3 = new AWS.S3({apiVersion: '2006-03-01'});
const aws_config = require('../aws-config.json');

module.exports.listarBucket = function(callback) {
	try {
		s3.listBuckets(function(error, data) {
			if(error) {
				callback(error, null);
			}else {
				callback(null, data);
			}
		});

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.getS3Configuration = function(callback) {
	callback(null, s3.endpoint);
}

module.exports.uploadObjetoBucket = function(bucket, key, stream, callback) {
	var default_bucket = null;
	var params = {
		Key: '',
		ACL: 'private',
		Bucket: '',
		Body: ''
	};

	try {
		if(key==null || stream==null) {
			console.error('Parâmetros inválidos para upload')
			callback(null, new Error('Parâmetros inválidos para upload'));
		}else {
			default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

			params.Bucket = default_bucket;
			params.Key = key;
			params.Body = new Buffer(stream);

			s3.putObject(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);
				}else {
					callback(null, data);
				}
			});
		}

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.listarObjetosBucket = function(bucket, callback) {
	var default_bucket = null;
	var params = {
		Bucket: ''
	};

	try {
		default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

		params.Bucket = default_bucket;

		s3.listObjects(params, function(error, data) {
			if(error) {
				console.error(error.stack);
				callback(error, null);
			}else {
				callback(null, data);
			}
		});

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.alterarACLObjeto = function(bucket, key, acl, callback) {
	var default_bucket = null;
	var params = {
		Bucket: '',
		Key:'',
		ACL: ''
	};

	try {
		if(key==null || acl==null) {

		}else {
			default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

			params.Bucket = default_bucket;
			params.Key = key;
			params.ACL = acl;

			s3.putObjectAcl(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);
				}else {
					callback(null, data);
				}
			});
		}
	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.getBucketACL = function(bucket, callback) {
	var default_bucket = null;
	var params = {
		Bucket: ''
	};

	try {
		default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

		params.Bucket = default_bucket;

		s3.getBucketAcl(params, function(error, data) {
			if(error) {
				console.error(error.stack);
				callback(error, null);
			}else {
				callback(null, data);
			}
		});

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.excluirBucket = function(bucket, callback) {
	var params = {
		Bucket: '',
	};

	try {
		if(bucket==null) {
			console.error('Bucket não pode ser null')
			callback(null, new Error('Bucket não pode ser null'));

		}else {
			params.Bucket = bucket;

			s3.deleteBucket(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);

				}else {
					callback(null, JSON.stringify(data));
				}
			});
		}
	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.criarBucket = function(bucket, callback) {
	var params = {
		Bucket: '',
	};

	try {
		if(bucket==null) {
			console.error('Bucket não pode ser null')
			callback(null, new Error('Bucket não pode ser null'));

		}else {
			params.Bucket = bucket;

			s3.createBucket(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);

				}else {
					callback(null, JSON.stringify(data));
				}
			});
		}
	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.deleteObject = function(bucket, key, callback) {
	var default_bucket = null;
	var params = {
		Key: '',
		Bucket: ''
	};

	try {
		default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

		if(key==null) {
			console.error('Chave não pode ser null')
			callback(null, new Error('Chave não pode ser null'));
		
		}else {
			params.Key = key;
			params.Bucket = bucket;

			s3.deleteObject(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);

				}else {
					callback(null, JSON.stringify(data));
				}
			});
		}

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}

module.exports.getObject = function(bucket, key, callback) {
	var default_bucket = null;
	var params = {
		Key: '',
		Bucket: ''
	};

	try {
		default_bucket = bucket==null ? aws_config.s3['bucket'] : bucket;

		if(key==null) {
			console.error('Chave não pode ser null')
			callback(null, new Error('Chave não pode ser null'));
		
		}else {
			params.Key = key;
			params.Bucket = bucket;

			s3.getObject(params, function(error, data) {
				if(error) {
					console.error(error.stack);
					callback(error, null);

				}else {
					callback(null, data);
				}
			});
		}

	}catch(e) {
		console.error(e);
		callback(null, e);
	}
}
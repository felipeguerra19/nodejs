# Gerenciamento de Conteúdo Estático

O projeto a seguir destina-se ao uso por parte dos desenvolvedores que possuem a necessidade de hospedar 
o conteúdo estático de uma aplicação (imagens, CSS, Javascript, flash, etc) no S3.

As seguintes premissas são necessárias para utilizar a biblioteca:

- [x] [NodeJs] (https://nodejs.org/en/download/) instalado
- [x] Credenciais da AWS com as políticas necessárias para se manusear o S3:
  
Ex.:
**config.json**
```
{ 
	"accessKeyId": "XXXXXXXXXXXXXXXXXXX",  
	"secretAccessKey": "PncGT9oTFT9KtvgEaP6Dycy_09807986574536L15",
	"region": "us-east-1"
}
```

## Criando e utilizando os objetos no S3 como conteúdo estático

O primeiro passo, após assegurar-se que as credenciais estão ok, é criar e/ou utilizar um bucket no S3 para hospedar as imagens, CSS, JSS, etc. Nesse sentido, através do script **use-case-test.js**, crie um bucket (nesse caso estou utilizando o *inovati-static-content*).

![Image AWS S3]
(files/images/novo-bucket.png)

Em seguida, basta realizar o upload da imagem para o bucket criado anteriormente. Nesse exemplo, fiz o upload das imagens que se encontram nesse tutorial.
Ex.:


```
function realizarUploadArquivo() {
	fs.readFile('files/images/novo-bucket.png', 'utf8', function(err, data) {
		if(err) {
			console.error(err);
			return;
		}else {
			console.info(data);

			app.uploadObjetoBucket('inovati-static-content', 'images/novo-bucket.png', 
								new Buffer(data), function(err, result) {
				if(err) {
					console.error(err);
					return;
				}else {
					console.info(result);
				}
			});
		}
	});
}
```

**Resultado:**

![Image AWS S3]
(files/images/upload-imagem.png)

Ao realizar o upload de um arquivo para o S3, a política de acesso padrão ao mesmo deve ser *private*, ou seja, somente é possível manipulá-lo através do console da AWS.

![Image AWS S3]
(files/images/upload-success.png)

Entretanto, o propósito é garantir que o arquivo estará hospedado no S3 como conteúdo estático, sendo acessível publicamente através do protocolo HTTP/HTTPS.

![Image AWS S3]
(files/images/forbidden-access-image.png)

Nesse exemplo, o código abaixo concedeu os privilégios necessários para o arquivo tornar-se público:

```
function alterarACLArquivo() {
	app.alterarACLObjeto('inovati-static-content', 'images/novo-bucket.png', 
						'public-read', function(err, data) {
		if(err) {
			console.error(err);
			return;
		}else {
			console.info(data);
		}
	});
}
```

**Resultado**
![Image AWS S3]
(files/images/allowed-access-image.png)


Através de um simples arquivo HTML, é possível verificar a imagem sendo exibida:

![Image AWS S3]
(files/images/html-static-content.png)

Ex.:
```
 	<img width="800" height="500" src="https://s3.amazonaws.com/inovati-static-content/images/novo-bucket.png" alt="novo-bucket.png" />
```


### Para consultar os dados de acesso do seu bucket, basta executar a função:

```
app.getS3Configuration(function(err, data) {
	console.info(data);
});
```

**Resultado**

```
Endpoint {
  protocol: 'https:',
  host: 's3.amazonaws.com',
  port: 443,
  hostname: 's3.amazonaws.com',
  pathname: '/',
  path: '/',
  href: 'https://s3.amazonaws.com/' }
```

Um exemplo de utilização do arquivo hospedado no S3 seria: *'https://s3.amazonaws.com/<seu-bucket>/<key>'*


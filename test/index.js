'use strict';

const Code = require('code');   // assertion library
const Lab = require('lab');
const lab = exports.lab = Lab.script();
const src = require('../bin/manager');

lab.describe('::::::Test StaticContentHandler ::::::', () => {
	lab.it('=> Configurações do S3 Bucket', (done) => {
		src.getS3Configuration(function(error, result) {
			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Listar Bucket', (done) => {
		src.listarBucket(function(error, result) {
			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Consultar Chave e Bucket igual a NULL', (done) => {
		src.getObject(null, null, function(error, result) {
			if(error)
				console.error('Deu erro: ' + error);

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Consultar Chave no S3', (done) => {
		src.getObject('abastecimentoluiza', 'prevision/', function(error, result) {
			if(error)
				console.error(error);
			else
				console.info('TESTE OK: ' + result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Consultar Permissões do Bucket Padrão no S3', (done) => {
		src.getBucketACL('abastecimentoluiza', function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Consultar Objetos do Bucket Padrão no S3', (done) => {
		src.listarObjetosBucket('abastecimentoluiza', function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Upload de Objeto NULL no Bucket Padrão no S3', (done) => {
		src.uploadObjetoBucket(null, null, null, function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Upload de Objeto correto no Bucket Padrão no S3', (done) => {
		src.uploadObjetoBucket('abastecimentoluiza-hml', 'teste.txt', '1000000', function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Remover de Objeto correto no Bucket Padrão no S3', (done) => {
		src.deleteObject('abastecimentoluiza-hml', 'teste.txt', function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Alterar ACL de Objeto correto no Bucket Padrão no S3', (done) => {
		src.alterarACLObjeto('abastecimentoluiza-hml', 'teste.txt', 'public-read', function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Criar Bucket no S3', (done) => {
		src.criarBucket('static-content',  function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});

	lab.it('=>Excluir Bucket no S3', (done) => {
		src.excluirBucket('static-content',  function(error, result) {
			if(error)
				console.error(error);
			else
				console.warn(result)

			Code.expect(result).to.not.equal(null);
		});
		done();
	});
})
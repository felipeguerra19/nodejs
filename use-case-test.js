'use strict';
const fs = require('fs');
const app = require('./index.js');
const path = require('path');

var archive = 'files/images/';
var archive_name = 'novo-bucket.png';

function criarBucket() {
	if(validarBucketInexistente()===false) {
		app.criarBucket('inovati-static-content', function(err, result) {
			if(err)
				return;
			else {
				console.info('Bucket criado com sucesso! ' + result);
			}
		});
	}
	realizarUploadArquivo();
};

function validarBucketInexistente() {
	var resp = true;

	app.listarBucket(function(err, result) {
		if(err) {
			console.error(err);
			return resp;
		}else {
			var buckets = result['Buckets'];
			
			buckets.forEach(function(item) {
				if('inovati-static-content'===item['Name']) {
					resp = true;
					console.info('['+resp+']Bucket encontrado: ' + JSON.stringify(item['Name']));
				}
			});

			return resp;	
		}
	});
}

function realizarUploadArquivo() {
	removerImagem();

	app.listarObjetosBucket('inovati-static-content', function(err, data) {
		if(err) {
			console.error(err);
			return;
		}else {
			var content_bucket = data['Contents'];
			
			content_bucket.forEach(function(item) {
				if(item['Key']!=null && item['Key'].indexOf(archive_name)>-1) {
					return;	
				}else {
					fs.readFile(path.join(__dirname, archive+archive_name), 
									'utf8', function(err, data) {
						if(err) {
							console.error(err);
							return;
						}else {
							console.info('Upload: ' + path.join(__dirname, archive+archive_name));
							
							app.uploadObjetoBucket('inovati-static-content', 'images/novo-bucket.png', 
												data, function(err, result) {
								if(err) {
									console.error(err);
									return;
								}else {
									console.info(result);

									alterarACLArquivo();
								}
							});
						}
					});
				}
			})
		}
	});
}

function alterarACLArquivo() {
	app.alterarACLObjeto('inovati-static-content', 'images/novo-bucket.png', 
						'public-read', function(err, data) {
		if(err) {
			console.error('Erro de Alterar ACL:' + err);
			return;
		}else {
			console.info(data);

			app.excluirBucket('inovati-static-content', function(err, data) {
				if(err) {
					console.error('Erro ao excluir o Bucket:' + err);
					return;
				}else {
					console.error('Bucket excluído com sucesso:' + data);
				}
			})
		}
	});
}

function removerImagem() {
	app.deleteObject('inovati-static-content', 'images/novo-bucket.png', function(err, data) {
		if(err) {
			console.error(err);
			return;
		}else {
			console.info(data);
		}
	});
}

criarBucket();

app.getS3Configuration(function(err, data) {
	console.info(data);
});
